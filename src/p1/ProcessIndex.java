package p1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.IOContext;

import p1.ProcessIndex.ResultTFIDF;

public class ProcessIndex {

	/**
	 * Clase que guarda los tipos de consulta que pueden darse.
	 *
	 */

	private static class Consulta {
		enum Clase {
			BESTIDF, POORIDF, BESTTFIDF, POORTFIDF, DELDOCSTERM, DELDOCSQUERY, DOCBODY, DOCTITLE
		}
	}

	/**
	 * Clase para guardar los terminos y sus DF asociados
	 *
	 */

	public class ResultIDF implements Comparable<ResultIDF> {

		private final String termino;
		private final int DF;
		private double IDF;

		public ResultIDF(String termino, int dF, int nDocs) {
			super();
			this.termino = termino;
			this.DF = dF;
			setIDF(nDocs);

		}

		public String getTermino() {
			return termino;
		}

		public int getDF() {
			return DF;
		}

		public double getIDF() {
			return IDF;
		}

		public void setIDF(int numDocs) {
			IDF = Math.log((double) numDocs / this.DF);
		}

		@Override
		public String toString() {
			return " [termino=" + termino + ", DF=" + DF + ", IDF=" + IDF + "]";
		}

		@Override
		public int compareTo(ResultIDF other) {

			if (this.IDF < other.IDF)
				return 1;
			else if (this.IDF > other.IDF)
				return -1;

			if (this.DF < other.DF)
				return 1;
			else if (this.DF > other.DF)
				return -1;

			return this.termino.compareTo(other.getTermino());

		}

	}

	/**
	 * Clase para guardar los resultados con su TF asociada
	 *
	 */

	public class ResultTFIDF implements Comparable<ResultTFIDF> {

		private final int docId;
		private final int TF;
		private double tfidf;
		private ResultIDF IDF;

		public ResultTFIDF(String termino, int dF, int tf, int docId, int nDocs) {
			this.IDF = new ResultIDF(termino, dF, nDocs);
			this.TF = tf;
			this.docId = docId;
			this.setTfidf();

		}

		public int getTF() {
			return TF;
		}

		@Override
		public String toString() {
			return " [doc=" + docId + ", termino=" + this.IDF.getTermino() + ", TF=" + TF + ", DF=" + this.IDF.getDF()
					+ ", IDF=" + this.IDF.getIDF() + ", tfidf=" + tfidf + "]";

		}

		public int getDocId() {
			return docId;
		}

		public double getTfidf() {
			return tfidf;
		}

		public void setTfidf() {
			// this.tfidf = (1 + Math.log((double) this.getDF())) *
			// this.getTF();
			this.tfidf = this.TF * this.IDF.getIDF();
		}

		public String getTermino() {
			return IDF.termino;
		}

		public int getDF() {
			return IDF.DF;
		}

		public double getIDF() {
			return IDF.IDF;
		}

		@Override
		public int compareTo(ResultTFIDF other) {

			if (this.tfidf < other.tfidf)
				return 1;
			else if (this.tfidf > other.tfidf)
				return -1;

			if (this.getIDF() < other.getIDF())
				return 1;
			else if (this.getIDF() > other.getIDF())
				return -1;

			if (this.getDocId() < other.getDocId())
				return 1;
			else if (this.getDocId() > other.getDocId())
				return -1;

			return this.getTermino().compareTo(other.getTermino());

		}

	}

	/**
	 * Clase para guardar y procesar los parametros
	 *
	 */

	public class Parameters {

		String indexPath;
		String indexPathOut;
		String term;
		String queryField;
		int topN, bottomN, nThreads;
		ProcessIndex.Consulta.Clase type;

		public Parameters() {

			this.indexPath = "";
			this.indexPathOut = "";
			this.queryField = "";
			this.topN = -1;
			this.bottomN = -1;

		}

		public String getIndexPath() {
			return indexPath;
		}

		public void setIndexPath(String indexPath) {
			this.indexPath = indexPath;
		}

		public String getQueryField() {
			return queryField;
		}

		public void setQueryField(String queryField) {
			this.queryField = queryField;
		}

		public String getIndexPath_out() {
			return indexPathOut;
		}

		public void setIndexPath_out(String indexPath_out) {
			this.indexPathOut = indexPath_out;
		}

		public String getTerm() {
			return term;
		}

		public void setTerm(String term) {
			this.term = term;
		}

		public int getTopN() {
			return topN;
		}

		public void setTopN(int topN) {
			this.topN = topN;
		}

		public int getBottomN() {
			return bottomN;
		}

		public void setBottomN(int bottomN) {
			this.bottomN = bottomN;
		}

		public ProcessIndex.Consulta.Clase getType() {
			return type;
		}

		public void setType(ProcessIndex.Consulta.Clase type) {
			this.type = type;
		}

		public void processParameters(String[] paramList) {

			System.out.println("Procesando parametros: ");

			for (int i = 0; i < paramList.length; i++) {

				if (paramList[i].equals("-indexin")) {
					indexPath = paramList[i + 1];
					i++;
					System.out.println("\tDirectorio del index: " + indexPath);

				} else if (paramList[i].equals("-indexout")) {
					indexPathOut = paramList[i + 1];
					i++;
					System.out.println("\tDirectorio del indexout: " + indexPathOut);

				} else if (paramList[i].equals("-deldocsterm")) {
					queryField = paramList[i + 1];
					term = paramList[i + 2];
					type = Consulta.Clase.DELDOCSTERM;
					i += 2;

				} else if (paramList[i].equals("-deldocsquery")) {
					queryField = paramList[i + 1];

					type = Consulta.Clase.DELDOCSQUERY;
					i++;

				}

				else if (paramList[i].equals("-best_idfterms")) {
					queryField = paramList[i + 1];
					topN = Integer.parseInt(paramList[i + 2]);
					type = Consulta.Clase.BESTIDF;
					i += 2;
					System.out.println("\tIDF de " + queryField + " de los mejores " + topN + " terminos");

				} else if (paramList[i].equals("-poor_idfterms")) {
					queryField = paramList[i + 1];
					bottomN = Integer.parseInt(paramList[i + 2]);
					type = Consulta.Clase.POORIDF;
					i += 2;
					System.out.println("\tIDF de " + queryField + " de los peores " + topN + " terminos");
				}

				else if (paramList[i].equals("-best_tfidfterms")) {
					queryField = paramList[i + 1];
					topN = Integer.parseInt(paramList[i + 2]);
					type = Consulta.Clase.BESTTFIDF;
					i += 2;
					System.out.println("\tTFIDF de " + queryField + " de los mejores " + topN + " terminos");

				} else if (paramList[i].equals("-poor_tfidfterms")) {
					queryField = paramList[i + 1];
					bottomN = Integer.parseInt(paramList[i + 2]);
					type = Consulta.Clase.POORTFIDF;
					i += 2;
					System.out.println("\tTFIDF de " + queryField + " de los peores " + bottomN + " terminos");

				} else if (paramList[i].equals("-mostsimilardoc_body")) {
					topN = Integer.parseInt(paramList[i + 1]);
					nThreads = Integer.parseInt(paramList[i + 2]);
					type = Consulta.Clase.DOCBODY;
					i += 2;
					System.out.println("\tMost similiar documento body de los mejores " + topN + " terminos con "
							+ nThreads + "hilos");

				} else if (paramList[i].equals("-mostsimilardoc_title")) {
					nThreads = Integer.parseInt(paramList[i + 1]);
					type = Consulta.Clase.DOCTITLE;
					i++;
					System.out.println("\tMost similiar documento title con " + nThreads + " hilos");

				}

			}
		}

	}

	/**
	 * Metodo para buscar en el indice con los parametros ya parseados que le
	 * pasan desde consola.
	 * 
	 * @param reader
	 * @param parametros
	 * @param analyzer
	 */

	public void search(DirectoryReader reader, Parameters parametros, IndexWriter writer, Analyzer analyzer) {

		switch (parametros.getType()) {

		case DELDOCSTERM:
			this.deldocsterm(reader, writer, parametros.getQueryField(), parametros.getIndexPath_out(),
					parametros.getTerm());
			break;
		case DELDOCSQUERY:
			this.deldocsquery(reader, writer, parametros.getQueryField(), parametros.getIndexPath_out(),
					parametros.getTerm());
			break;

		case BESTIDF:
			this.processIDF(reader, parametros.getQueryField(), parametros.getTopN(), true);
			break;
		case POORIDF:
			this.processIDF(reader, parametros.getQueryField(), parametros.getBottomN(), false);
			break;
		default:
			break;
		case BESTTFIDF:
			try {
				this.processIDFTF(reader, parametros.getQueryField(), parametros.getTopN(), true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case POORTFIDF:
			try {
				this.processIDFTF(reader, parametros.getQueryField(), parametros.getBottomN(), false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case DOCTITLE:
			try {
				SimilarThreadPool.similarDocTitle(reader, analyzer, writer, parametros.nThreads, 0, 0,
						parametros.indexPathOut);
			} catch (IOException e) {

				e.printStackTrace();
			}
			break;

		case DOCBODY:
			try {
				SimilarThreadPool.similarDocTitle(reader, analyzer, writer, parametros.nThreads, 1,
						parametros.getTopN(), parametros.getIndexPath());
			} catch (IOException e) {

				e.printStackTrace();
			}
			break;
		}

	}

	private void deldocsterm(DirectoryReader reader, IndexWriter writer, String queryField, String indexout_path,
			String term) {

		Term t = new Term(queryField, term);

		try {
			writer.deleteDocuments(t);

			writer.commit();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void deldocsquery(DirectoryReader reader, IndexWriter writer, String queryField, String indexout_path,
			String term) {

		Analyzer analyzer = new StandardAnalyzer();

		String[] fields = { "body", "tittle", "date", "dateline", "seqDoc", "topic", "pathSgm", "thread", "host" };
		BooleanClause.Occur[] flags = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD,
				BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD,
				BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD,
				BooleanClause.Occur.SHOULD };
		try {
			Query query = MultiFieldQueryParser.parse(queryField, fields, flags, analyzer);
			writer.deleteDocuments(query);

			writer.commit();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void processIDF(IndexReader reader, String queryField, int ndocuments, boolean top) {

		int numDocs = reader.numDocs();
		Terms terms = null;
		Fields fields;
		TermsEnum termsEnum = null;

		try {
			fields = MultiFields.getFields(reader);
			ArrayList<ResultIDF> resList = new ArrayList<ResultIDF>();

			for (String field : fields) {

				if (field.equals(queryField)) {

					terms = fields.terms(field);

					termsEnum = terms.iterator();

					while (termsEnum.next() != null) {
						String tt = termsEnum.term().utf8ToString();

						ResultIDF tupla = new ResultIDF(tt, termsEnum.docFreq(), numDocs);
						resList.add(tupla);

					}

				}
			}

			Collections.sort(resList);
			if (top) {

				System.out.println(
						"\n Mostrando los mejores " + ndocuments + " terminos  " + "del Field " + queryField + "\n");

			} else {
				System.out.println(
						"\n Mostrando los peores " + ndocuments + " terminos " + "del Field " + queryField + "\n");
				Collections.reverse(resList);

			}

			for (int i = 0; i < ndocuments; i++) {
				System.out.println("\t  ResultIDF= " + i + resList.get(i));

			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	ArrayList<ResultTFIDF> getTFIDFarrayOFaDocOrdered(ArrayList<ResultTFIDF> arrayOriginal, int docId, int topN) {

		ArrayList<ResultTFIDF> resList = new ArrayList<ResultTFIDF>();
		int x = 0;
		int countTerms = 0;

		while (x < arrayOriginal.size() && countTerms < topN) {

			if (arrayOriginal.get(x).docId == docId) {
				resList.add(countTerms, arrayOriginal.get(x));
				countTerms++;
			}
			x++;
		}
		return resList; // devuelve un array con los mejores topN terminos del
						// docId ordenados por TFIDF
	}

	public ArrayList<ResultTFIDF> processIDFTF(DirectoryReader reader, String queryField, int ndocuments, boolean top)
			throws IOException {

		// DirectoryReader reader2 = (DirectoryReader) reader;
		ArrayList<ResultTFIDF> resList = new ArrayList<ResultTFIDF>();

		for (final LeafReaderContext leaf : reader.leaves()) {
			// Print leaf number (starting from zero)

			// Create an AtomicReader for each leaf
			// (using, again, Java 7 try-with-resources syntax)
			try (LeafReader leafReader = leaf.reader()) {

				// Get the fields contained in the current segment/leaf
				final Fields fields = leafReader.fields();

				for (final String field : fields) {

					if (field.equals(queryField)) {

						final Terms terms = fields.terms(field);
						final TermsEnum termsEnum = terms.iterator();

						while (termsEnum.next() != null) {

							Term term = new Term(queryField, termsEnum.term());
							PostingsEnum postingsEnum = leafReader.postings(term);

							int doc;

							String tt = termsEnum.term().utf8ToString();
							int df = termsEnum.docFreq();

							while ((doc = postingsEnum.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {

								int tf = postingsEnum.freq();
								if (tt.equals("smoke"))
									System.out.println("SMOOKE MATAAAAAAAAAA  = " + tf);
								int docId = postingsEnum.docID();

								resList.add(new ResultTFIDF(tt, df, tf, docId, reader.numDocs()));

							}

						}

					}

				}

			}

		}

		Collections.sort(resList);

		if (!top) {
			Collections.reverse(resList);
			System.out.println("\n Mostrando los peores " + ndocuments + " TFIDF " + "del Field " + queryField + "\n");

		} else
			System.out.println("\n Mostrando los mejores " + ndocuments + " TFIDF " + "del Field " + queryField + "\n");

		for (int i = 0; i < ndocuments; i++) {

			// if (resList.get(i).equals("achievable"))
			System.out.println("\t  ResultTFIDF= " + i + resList.get(i));

		}

		return resList;

	}

	private void similarDocBody(DirectoryReader reader, Analyzer analyzer, IndexWriter writer, Parameters parametros)
			throws IOException {

		// IndexSearcher s = new IndexSearcher(reader);
		String queryString = "";
		ArrayList<ResultTFIDF> results = new ArrayList();
		ArrayList<ResultTFIDF> resultsDocId = new ArrayList();
		String[] fields = { "tittle", "body" };
		BooleanClause.Occur[] flags = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD };
		Query query = null;
		Field SimTitle = new TextField("SimTitle", "", Field.Store.YES);
		Field SimTitle2 = new TextField("SimTitle2", "", Field.Store.YES);
		Field SimBody = new TextField("SimBody", "", Field.Store.YES);
		Field SimQuery = new TextField("SimQuery", "", Field.Store.YES);
		Field SimPathSgm = new StringField("SimPathSgm", "", Field.Store.YES);

		try {
			results = this.processIDFTF(reader, "body", parametros.getTopN(), true);

		} catch (IOException e) {

			e.printStackTrace();
		}
		DirectoryReader indexReader = DirectoryReader.open(FSDirectory.open(Paths.get(parametros.getIndexPath())));
		IndexSearcher s = new IndexSearcher(indexReader);
		int i = 0;
		for (i = 0; i < indexReader.numDocs(); i++) {
			queryString = "";
			resultsDocId = getTFIDFarrayOFaDocOrdered(results, i, parametros.getTopN());
			System.out.println("tama�o: " + resultsDocId.size());
			for (int x = 0; ((x < parametros.getTopN()) && (x < resultsDocId.size())); x++) {
				if (resultsDocId.get(x) != null)
					queryString += " " + resultsDocId.get(x).getTermino();

			}
			System.out.println("query: " + queryString);
			try {
				if (!queryString.equals(""))
					query = MultiFieldQueryParser.parse(QueryParser.escape(queryString), fields, flags, analyzer);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// ahora indexamos...
			TopDocs top = null;
			if (!queryString.equals("")) {

				top = s.search(query, 2);
			}
			// copiaremos el mejor como el segundo, pero de no haber ponemos el
			// primero (si mismo..), y sino un ""
			int mejor = 0;
			if (queryString.equals("") || ((!queryString.equals("")) && (top.totalHits == 0))) {
				SimTitle.setStringValue("");
				SimTitle2.setStringValue("");
				SimBody.setStringValue("");
				SimPathSgm.setStringValue("");
				SimQuery.setStringValue("");
			} else if (top.totalHits > 1) {

				mejor = top.scoreDocs[1].doc;

				SimTitle.setStringValue(s.doc(top.scoreDocs[0].doc).getField("tittle").toString());
				SimTitle2.setStringValue(s.doc(mejor).getField("tittle").toString());
				SimBody.setStringValue(s.doc(mejor).getField("body").toString());
				SimPathSgm.setStringValue(s.doc(mejor).getField("pathSgm").toString());
				SimQuery.setStringValue(queryString);
			} else {
				mejor = top.scoreDocs[0].doc;

				SimTitle.setStringValue(s.doc(mejor).getField("tittle").toString());
				SimTitle2.setStringValue("");
				SimBody.setStringValue(s.doc(mejor).getField("body").toString());
				SimPathSgm.setStringValue(s.doc(mejor).getField("pathSgm").toString());
				SimQuery.setStringValue(queryString);
			}

			Document doc = indexReader.document(i);

			doc.add(SimTitle);
			doc.add(SimTitle2);
			doc.add(SimBody);
			doc.add(SimPathSgm);

			writer.addDocument(doc);

		}
		indexReader.close();

	}

	public static void borrarContenidoDirectorio(String direccion) {

		File f = new File(direccion);
		File[] ficheros = f.listFiles();
		for (int x = 0; x < ficheros.length; x++) {
			if (ficheros[x].isDirectory()) {
				borrarContenidoDirectorio(ficheros[x].getPath());
			}
			ficheros[x].delete();
		}
	}

	public static void main(String argv[]) {

		// IndexReader reader = null;
		DirectoryReader indexReader = null;
		IndexWriter writer = null;
		Directory dirx = null;

		System.out.println(" RI 2017. ProcessIndex \n");

		ProcessIndex process = new ProcessIndex();

		Parameters parametros = process.new Parameters();
		parametros.processParameters(argv);

		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig iwcx = new IndexWriterConfig(analyzer);
		if ((parametros.getType() == Consulta.Clase.DELDOCSTERM)
				|| (parametros.getType() == Consulta.Clase.DELDOCSQUERY)) {
			iwcx.setOpenMode(OpenMode.APPEND);
		} else {
			iwcx.setOpenMode(OpenMode.CREATE);
		}
		try {

			dirx = FSDirectory.open(Paths.get(parametros.indexPath));

			if (!(parametros.getIndexPath_out().equals(""))) {
				borrarContenidoDirectorio(parametros.indexPathOut);
				Directory dir_out = null;
				dir_out = FSDirectory.open(Paths.get(parametros.indexPathOut));

				for (String file : dirx.listAll()) {

					dir_out.copyFrom(dirx, file, file, IOContext.DEFAULT);
				}
				writer = new IndexWriter(dir_out, iwcx);

			} else {

				writer = new IndexWriter(dirx, iwcx);

			}

			indexReader = DirectoryReader.open(FSDirectory.open(Paths.get(parametros.getIndexPath())));

		} catch (IOException e) {

			e.printStackTrace();
		}

		process.search(indexReader, parametros, writer, analyzer);

		System.out.println("\nFin ejecucion.");

	}

}
