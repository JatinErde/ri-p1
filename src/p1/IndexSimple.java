package p1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

public class IndexSimple {

	public static void indexDocs(final IndexWriter writer, Path path) throws IOException {

		String thread = Thread.currentThread().getName();
		String host = InetAddress.getLocalHost().getHostName();

		if (Files.isDirectory(path)) {
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try {

						indexDoc(writer, file, attrs.lastModifiedTime().toMillis(), thread, host);
						
					} catch (IOException ignore) {
						// don't index files that can't be read.
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} else {
			indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis(), thread, host);
		}
	}

	/** Indexes a single document */
	public static void indexDoc(IndexWriter writer, Path filex, long lastModified, String thread, String host)
			throws IOException {

		
		File file = new File(filex.toString());
		ListIterator<List<String>> iter = generar_listaReuters(file);
		Integer secDoc = new Integer(1);

		/* Por eficiencia creo cada campo antes */

		Field tittleField = new TextField("tittle", "", Field.Store.YES);
		Field bodyField = new TextField("body", "", Field.Store.YES);
		
		
		Field dLineField = new StringField("dateline", "", Field.Store.YES);
		Field dateField = new StringField("date", "", Field.Store.YES);
		Field seqDocField = new StringField("seqDoc", "", Field.Store.YES);
		Field threadField = new StringField("thread", thread, Field.Store.YES);
		Field hostField = new StringField("host", host, Field.Store.YES);
		Field pathField = new StringField("pathSgm",filex.toString(), Field.Store.YES);
	

		/* SeqDocNumber */

		while (iter.hasNext()) {

			List<String> reuterx = iter.next();
			Document doc = new Document();

			/* Tittle */

			if (reuterx.get(0) != null) {
				tittleField.setStringValue(reuterx.get(0));
				doc.add(tittleField);
			}

			/* Body */

			if (reuterx.get(1) != null) {
				bodyField.setStringValue(reuterx.get(1));
				doc.add(bodyField);
			}

			/* topics */

			if (reuterx.get(2) != null) {

				StringTokenizer topicList = new StringTokenizer(reuterx.get(2));

				while (topicList.hasMoreTokens()) {
					String topic = topicList.nextToken();
					Field topicsField = new TextField("topic", topic, Field.Store.YES);
					doc.add(topicsField);
				}
			}

			/* Date line */

			if (reuterx.get(3) != null) {
				dLineField.setStringValue(reuterx.get(3));
				doc.add(dLineField);

			}

			/* Data */

			if (reuterx.get(4) != null) {

				try {
					Date entrydate = new SimpleDateFormat("d-MMM-yyyy HH:mm:ss.SS", Locale.US).parse(reuterx.get(4));
					dateField.setStringValue(DateTools.dateToString(entrydate, DateTools.Resolution.SECOND));
					doc.add(dateField);

				} catch (ParseException e) {

					e.printStackTrace();
				}

				
				 Date fechaIndexacion = new Date();
		         Field indexDate = new StringField("indexdate", DateTools.dateToString(fechaIndexacion, DateTools.Resolution.MILLISECOND), Field.Store.YES);
		         doc.add(indexDate);
				/* seqDocField, threads, host */

				seqDocField.setStringValue(secDoc.toString());
				doc.add(seqDocField);
				
				doc.add(threadField);
				doc.add(hostField);	
				doc.add(pathField);

			}

			writer.addDocument(doc);
			secDoc++;
		}

		/*
		 * System.out.println("updating " + file); writer.updateDocument(new
		 * Term("path", file.toString()), doc);
		 */
	}

	public static ListIterator<List<String>> generar_listaReuters(File file) throws IOException {
		FileInputStream fis = null;

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuffer fileContents = new StringBuffer();
		String line = null;

		line = br.readLine();

		while (line != null) {

			fileContents.append(line);
			fileContents.append("\n");

			line = br.readLine();
		}

		br.close();

		List<List<String>> docsinfo = Reuters21578Parser.parseString(fileContents);

		ListIterator<List<String>> iter = docsinfo.listIterator();

		return iter;

	}

}
