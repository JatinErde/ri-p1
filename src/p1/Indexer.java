package p1;


import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class Indexer {

	/**
	 * Esta clase permite parsear los parametros con los que se invoca el
	 * programa durante la ejercución los podemos recuperar invocando esta
	 * clase.
	 * 
	 * 
	 * @author Javier Ludeña & Fernando Pena
	 *
	 */
	private class Parameters {

		// Opciones de indexacion

		private String mode;
		private String indexPath;
		private String collPath;
		private List<String> colls;
		List<String> bd;

		/* Concurrencia 1 y 2 */
		private List<String> indexes1;
		private String indexes2;

		private Parameters() {

			/* Parametros Indexacion */

			this.mode = "";
			this.indexPath = "";
			this.collPath = "";
			this.colls = new ArrayList<String>();
			this.indexes1 = new ArrayList<String>();
			this.indexes2 = "";
			inicializebd();

		}

		private void inicializebd() {
			bd = new ArrayList<String>();

			bd.add("-openmode");
			bd.add("-index");
			bd.add("-indexes1");
			bd.add("-indexes2");
			bd.add("-coll");
			bd.add("-colls");

		}

		private boolean entradaReconocida(String valor) {

			return bd.contains(valor);

		}

		public void inicializeParameters(String[] paramList) {

			System.out.println("Procesando parametros: ");

			for (int i = 0; i < paramList.length; i++) {

				if (paramList[i].equals("-openmode")) {
					mode = paramList[i + 1];
					if (!(mode.contains("create") || mode.contains("append") || mode.contains("created_or_append"))) {
						System.err.println(
								"parametro openmode faltante o erroneo. FORMATO: -openmode [created|append|created_or_append]");
						System.exit(1);
					}
					i++;
				}

				else if (paramList[i].equals("-index")) {
					indexPath = paramList[i + 1];
					i++;
					System.out.println("\tDirectorio del index: " + indexPath);
				}

				else if (paramList[i].equals("-coll")) {

					collPath = paramList[i + 1];
					i++;
					System.out.println("\tDirectorio de los ficheros SGM: " + collPath);
				}

				else if (paramList[i].equals("-colls")) {
					int x = i + 1;
					int cont = 0;

					while ((x<paramList.length)&&(!entradaReconocida(paramList[x]))) {
						colls.add(cont, paramList[x]);
						cont++;
						x++;
						
					System.out.println("\tDirectorio de los ficheros SGM: " + collPath);
					}
					i = x - 1;
				}

				else if (paramList[i].equals("-indexes1")) {
					int x = i + 1;
					int cont = 0;

					while (!entradaReconocida(paramList[x])) {
						indexes1.add(cont, paramList[x]);
						cont++;
						x++;
					}
					i = x - 1;
				}

				else if (paramList[i].equals("-indexes2")) {
					indexes2 = paramList[i + 1];
					i++;

					System.out.println("\tDirectorio del index: " + indexes2);

				}

			}

		}

		public String getMode() {
			return mode;
		}

		public String getIndexPath() {
			return indexPath;
		}

		public String getCollPath() {
			return collPath;
		}

		public List<String> getColls() {
			return colls;
		}

		public List<String> getIndexes1() {
			return indexes1;
		}

		public String getIndexes2() {
			return indexes2;
		}
	}

	public void indexDocuments(Parameters parameters) {

		// Antes de nada inicializo Analizer, IndexWrite, OpenMode

		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		IndexWriter writer = null;

		Date start = new Date();

		switch (parameters.getMode()) {

		case "append":
			iwc.setOpenMode(OpenMode.APPEND);
			break;
		case "create_or_append":
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			break;
		default:
			iwc.setOpenMode(OpenMode.CREATE);
			break;
		}

		if (!parameters.getIndexPath().equals("")) {

			try {
				Directory dir = FSDirectory.open(Paths.get(parameters.getIndexPath()));
				writer = new IndexWriter(dir, iwc);

			} catch (IOException e) {
				e.printStackTrace();
			}

			if (!parameters.getCollPath().equals("")) {

				final Path docDir = Paths.get(parameters.getCollPath());
				if (!Files.isReadable(docDir)) {
					System.out.println("Document directory '" + docDir.toAbsolutePath()
							+ "' does not exist or is not readable, please check the path");
					System.exit(1);
				}

				try {
					IndexSimple.indexDocs(writer, docDir);

				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			else {

				// Caso en el que estamos en colls
				for (int x = 0; x < parameters.getColls().size(); x++) {

					final Path docDir = Paths.get(parameters.getColls().get(x));
					if (!Files.isReadable(docDir)) {
						System.out.println("Document directory '" + docDir.toAbsolutePath()
								+ "' does not exist or is not readable, please check the path");
						System.exit(1);
					}

					try {

						IndexSimple.indexDocs(writer, docDir);

					} catch (IOException e) {
						System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
					}
				}

			}

			try {
				writer.commit();
				writer.close();
			} catch (CorruptIndexException e) {
				System.out.println("Graceful message: exception " + e);
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Graceful message: exception " + e);
				e.printStackTrace();
			}

			Date end = new Date();
			System.out.println(end.getTime() - start.getTime() + " total milliseconds");

		}

		// caso Indexes

		else {

			if (!parameters.getIndexes2().equals("")) {

				try {
					indexes2(parameters);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			else {

				try {
					indexes1(parameters);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	/****
	 * INDEXER
	 * 
	 *
	 * @param argv
	 */

	private static void indexes2(Parameters params) throws IOException {
		// falta implementarla..
		Date start = new Date();

		// Optional: for better indexing performance, if you
		// are indexing many documents, increase the RAM
		// buffer. But if you do this, increase the max heap
		// size to the JVM (eg add -Xmx512m or -Xmx1g):
		//
		// iwc.setRAMBufferSizeMB(256.0);

		Analyzer analyzerx = new StandardAnalyzer();
		IndexWriterConfig iwcx = new IndexWriterConfig(analyzerx);

		switch (params.getMode()) {

		case "append":
			iwcx.setOpenMode(OpenMode.APPEND);
			break;
		case "create_or_append":
			iwcx.setOpenMode(OpenMode.CREATE_OR_APPEND);
			break;
		default:
			iwcx.setOpenMode(OpenMode.CREATE);
			break;
		}

		Directory dirx = FSDirectory.open(Paths.get(params.indexes2));
		IndexWriter writer = new IndexWriter(dirx, iwcx);
		int x = 0;
		for (x = 0; x < params.colls.size(); x++) {
			final Path docDir = Paths.get(params.colls.get(x));
			if (!Files.isReadable(docDir)) {
				System.out.println("Document directory '" + docDir.toAbsolutePath()
						+ "' does not exist or is not readable, please check the path");
				System.exit(1);
			}

			try {
				System.out.println("Indexing to directory '" + params.indexPath + "'...");

				ThreadPool.indexDocs(writer, docDir);

				//

			} catch (IOException e) {
				System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
			}
		}
		writer.close();
		Date end = new Date();
		System.out.println(end.getTime() - start.getTime() + " total milliseconds");

	}

	private static void indexes1(Parameters params) throws IOException {

		int x = 0;
		ArrayList<Directory> dirs = new ArrayList<Directory>();
		Date start = new Date();

		// Optional: for better indexing performance, if you
		// are indexing many documents, increase the RAM
		// buffer. But if you do this, increase the max heap
		// size to the JVM (eg add -Xmx512m or -Xmx1g):
		//
		// iwc.setRAMBufferSizeMB(256.0);

		Analyzer analyzerx = new StandardAnalyzer();
		IndexWriterConfig iwcx = new IndexWriterConfig(analyzerx);

		switch (params.getMode()) {

		case "append":
			iwcx.setOpenMode(OpenMode.APPEND);
			break;
		case "create_or_append":
			iwcx.setOpenMode(OpenMode.CREATE_OR_APPEND);
			break;
		default:
			iwcx.setOpenMode(OpenMode.CREATE);
			break;
		}
		Directory dirx = FSDirectory.open(Paths.get(params.indexes1.get(0)));
		IndexWriter writerF = new IndexWriter(dirx, iwcx);
		///
		for (x = 0; x < params.colls.size(); x++) {

			Directory dir = FSDirectory.open(Paths.get(params.indexes1.get(x)));
			dirs.add(dir);
		}
		///

		for (x = (params.colls.size() - 1); x >= 0; x--) {
			System.out.println("SIZEEEEEEEEEE-1:  " + x);
			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			System.out.println("param: " + Paths.get(params.indexes1.get(x)));
			switch (params.getMode()) {

			case "append":
				iwc.setOpenMode(OpenMode.APPEND);
				break;
			case "create_or_append":
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
				break;
			default:
				iwc.setOpenMode(OpenMode.CREATE);
				break;
			}
			IndexWriter writer;
			if (x == 0) {
				writer = writerF;
			} else {
				writer = new IndexWriter(dirs.get(x), iwc);
			}
			final Path docDir = Paths.get(params.colls.get(x));
			if (!Files.isReadable(docDir)) {
				System.out.println("Document directory '" + docDir.toAbsolutePath()
						+ "' does not exist or is not readable, please check the path");
				System.exit(1);
			}

			try {
				System.out.println("Indexing to directory '" + params.indexPath + "'...");

				ThreadPool.indexDocs(writer, docDir);

				//

			} catch (IOException e) {
				System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
			}

			if (x != 0) {
				writer.close();
				writerF.addIndexes(dirs.get(x));
			} else {
				writer.close();
			}

		}

		Date end = new Date();
		System.out.println(end.getTime() - start.getTime() + " total milliseconds");

	}

	public static void main(String argv[]) {

		System.out.println("RI 2017 - Indexer");

		Indexer index = new Indexer();

		/* Parse arguments */
		Parameters parameters = index.new Parameters();
		parameters.inicializeParameters(argv);

		/* Index starts */
		index.indexDocuments(parameters);

	}

}
