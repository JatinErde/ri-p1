package p1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;

/**
 * The Class ThreadPoolExample. It print the name of each subfolder using a
 * different thread.
 */
public class ThreadPool {

	/**
	 * This Runnable takes a folder and prints its path.
	 */
	public static class WorkerThread implements Runnable {

		private final Path filex;
		private final IndexWriter writer;
		private final long lastModified;

		public ListIterator<List<String>> generar_listaReuters(File file) {
			FileInputStream fis = null;

			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StringBuffer fileContents = new StringBuffer();
			String line = null;
			ListIterator<List<String>> iter = null;
			try {
				line = br.readLine();

				while (line != null) {// System.out.println("aqui9.1 valor de
										// line es: "+line); //line devuelve un
										// valor razonable..(no null...)
					fileContents.append(line);
					fileContents.append("\n");

					line = br.readLine();
				}

				br.close();

				List<List<String>> docsinfo = Reuters21578Parser.parseString(fileContents);
				iter = docsinfo.listIterator();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return iter;
		}

		public WorkerThread(IndexWriter writer, Path filex, long lastModified) {
			this.filex = filex;
			this.writer = writer;
			this.lastModified = lastModified;
		}

		/**
		 * This is the work that the current thread will do when processed by
		 * the pool. In this case, it will only print some information.
		 */
		@Override
		public void run() {
			try {
				
				String thread = Thread.currentThread().getName();
				String host = InetAddress.getLocalHost().getHostName();
				IndexSimple.indexDoc(writer, filex, lastModified,thread, host);

			} catch (Exception e) {
			
				e.printStackTrace();
			}

		}
	}

	static void indexDocs(final IndexWriter writer, Path path) throws IOException {

		/*
		 * Create a ExecutorService (ThreadPool is a subclass of
		 * ExecutorService) with so many thread as cores in my machine. This can
		 * be tuned according to the resources needed by the threads.
		 */
		final int numCores = Runtime.getRuntime().availableProcessors();
		final ExecutorService executor = Executors.newFixedThreadPool(numCores);

		/*
		 * We use Java 7 NIO.2 methods for input/output management. More info
		 * in: http://docs.oracle.com/javase/tutorial/essential/io/fileio.html
		 *
		 * We also use Java 7 try-with-resources syntax. More info in:
		 * https://docs.oracle.com/javase/tutorial/essential/exceptions/
		 * tryResourceClose.html
		 */
		if (Files.isDirectory(path)) {
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

					final Runnable worker = new WorkerThread(writer, file, attrs.lastModifiedTime().toMillis());
					/*
					 * Send the thread to the ThreadPool. It will be processed
					 * eventually.
					 */
					executor.execute(worker);
					return FileVisitResult.CONTINUE;
				}
			});
		} else {
			final Runnable worker = new WorkerThread(writer, path, Files.getLastModifiedTime(path).toMillis());
			/*
			 * Send the thread to the ThreadPool. It will be processed
			 * eventually.
			 */
			executor.execute(worker);
		}

		/*
		 * Close the ThreadPool; no more jobs will be accepted, but all the
		 * previously submitted jobs will be processed.
		 */
		executor.shutdown();

		/* Wait up to 1 hour to finish all the previously submitted jobs */
		try {
			executor.awaitTermination(1, TimeUnit.HOURS);
		} catch (final InterruptedException e) {
			e.printStackTrace();
			System.exit(-2);
		}

		System.out.println("Finished all threads");

	}

}