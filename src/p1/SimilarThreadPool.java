package p1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import p1.ProcessIndex.Parameters;
import p1.ProcessIndex.ResultTFIDF;


/**
 * The Class ThreadPoolExample. It print the name of each subfolder using a
 * different thread.
 */
public class SimilarThreadPool {

	/**
	 * This Runnable takes a folder and prints its path.
	 */
	public static class WorkerThread implements Runnable {

		private final DirectoryReader reader;
		private final IndexWriter writer;
		private final Analyzer analyzer;
		private final int primero;
		private final int ultimo;
		private final int tipo;
		private final int topN;
		private final String index_out;
		ProcessIndex p;
		ArrayList<ResultTFIDF> results;
		
		
		public WorkerThread(ArrayList<ResultTFIDF> results,DirectoryReader reader, Analyzer analyzer, IndexWriter writer, final int primero,final int ultimo,final int tipo,final int topN,String index_out) {
			this.reader = reader;
			this.writer = writer;
			this.analyzer = analyzer;
			this.primero = primero;
			this.ultimo = ultimo;
			this.tipo=tipo;
			this.topN=topN;
			this.index_out = index_out;
			this.p=new ProcessIndex();
			this.results=results;
			
			
			
		}

		/**
		 * This is the work that the current thread will do when processed by
		 * the pool. In this case, it will only print some information.
		 * @throws IOException 
		 */
		
		
		public void simBody() throws IOException{//tipo:1
			
			//IndexSearcher s = new IndexSearcher(reader);
			String queryString = "";
			
			ArrayList<ResultTFIDF> resultsDocId = new ArrayList();
			String[] fields = { "tittle", "body" };
			BooleanClause.Occur[] flags = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD };
			Query query =null;
			Field SimTitle = new TextField("SimTitle", "", Field.Store.YES);
			Field SimTitle2 = new TextField("SimTitle2", "", Field.Store.YES);
			Field SimBody = new TextField("SimBody", "", Field.Store.YES);
			Field SimBody2 = new TextField("SimBody2", "", Field.Store.YES);
			Field SimQuery = new TextField("SimQuery", "", Field.Store.YES);
			Field SimPathSgm = new StringField("SimPathSgm", "", Field.Store.YES);
			Field SimPathSgm2 = new StringField("SimPathSgm2", "", Field.Store.YES);
			DirectoryReader readerx = null;
			ProcessIndex p = new ProcessIndex();
			DirectoryReader indexReader = null;
			try {
				
				indexReader = DirectoryReader.open(FSDirectory.open(Paths.get(this.index_out)));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			IndexSearcher s = new IndexSearcher(indexReader);
			int i=0;
			for (i = primero; i < ultimo; i++) {
				queryString="";
				resultsDocId=this.p.getTFIDFarrayOFaDocOrdered(results,i,this.topN);
				//System.out.println("tama�o: "+resultsDocId.size());
				for (int x = 0; ((x < this.topN)&&(x<resultsDocId.size())); x++) {
					if (resultsDocId.get(x)!=null)
						queryString += " " + resultsDocId.get(x).getTermino();

				}
				//System.out.println("query: "+ queryString );
				try {
					if (!queryString.equals(""))
						query = MultiFieldQueryParser.parse(QueryParser.escape(queryString), fields, flags, analyzer);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				//ahora indexamos...
				TopDocs top=null;
				if (!queryString.equals("")){
					
					top = s.search(query, 2);
				}
				// copiaremos el mejor como el segundo, pero de no haber ponemos el primero (si mismo..), y sino un ""
				int mejor=0;
				if (queryString.equals("")||((!queryString.equals(""))&&(top.totalHits==0))){
					SimTitle.setStringValue("");
					SimTitle2.setStringValue("");
					SimBody.setStringValue("");
					SimBody2.setStringValue("");
					SimPathSgm.setStringValue("");
					SimPathSgm2.setStringValue("");
					SimQuery.setStringValue("");
				}else 
				if (top.totalHits>1){
					
					mejor = top.scoreDocs[1].doc;
					
					SimTitle.setStringValue(s.doc(top.scoreDocs[0].doc).getField("tittle").stringValue());
					SimTitle2.setStringValue(s.doc(mejor).getField("tittle").stringValue());
					SimBody.setStringValue(s.doc(top.scoreDocs[0].doc).getField("body").stringValue());
					SimBody2.setStringValue(s.doc(mejor).getField("body").stringValue());
					SimPathSgm.setStringValue(s.doc(top.scoreDocs[0].doc).getField("pathSgm").stringValue());
					SimPathSgm2.setStringValue(s.doc(mejor).getField("pathSgm").stringValue());
					SimQuery.setStringValue(queryString);
				}else  {
					mejor = top.scoreDocs[0].doc; 
					
					SimTitle.setStringValue(s.doc(mejor).getField("tittle").stringValue());
					SimTitle2.setStringValue("");
					SimBody.setStringValue(s.doc(mejor).getField("body").stringValue());
					SimBody2.setStringValue("");
					SimPathSgm.setStringValue(s.doc(mejor).getField("pathSgm").stringValue());
					SimPathSgm2.setStringValue("");
					SimQuery.setStringValue(queryString);
					}
				
				
				Document doc = indexReader.document(i);
				
				doc.add(SimTitle);
				doc.add(SimTitle2);
				doc.add(SimBody);
				doc.add(SimBody2);
				doc.add(SimPathSgm);
				doc.add(SimPathSgm2);
				doc.add(SimQuery);
				
				writer.addDocument(doc);
				
			}
			indexReader.close();
			
			
		}
		public void simDoc(){//tipo:0
			
			

				IndexSearcher s = new IndexSearcher(reader);
				String[] fields = { "tittle", "body" };
				BooleanClause.Occur[] flags = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD };
				int i = 0;
				
				Field  SimTitle = new TextField("SimTitle", "", Field.Store.YES);
				Field  SimTitle2 = new TextField("SimTitle2", "", Field.Store.YES);
				Field SimBody = new TextField("SimBody", "", Field.Store.YES);
				Field SimBody2= new TextField("SimBody2", "", Field.Store.YES);
				Field SimPathSgm = new StringField("SimPathSgm", "", Field.Store.YES);
				Field SimPathSgm2 = new StringField("SimPathSgm2", "", Field.Store.YES);
				
				for (i = primero; i < ultimo; i++) {

					// optenemos el titulo del doc
					String titulo;
					try {
						titulo = s.doc(i).get("tittle").toString();

						// realizamos la query para el titulo de este doc frente a los
						// titulos y bodys de los demas..
						Query query = null;
						
						try {

							
							if (!titulo.equals(""))
								query = MultiFieldQueryParser.parse(QueryParser.escape(titulo), fields, flags, analyzer);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
								e.printStackTrace();
						}
						// ahora optenemos el topDoc para la query..optenemos los dos
						// mejores..el primero sera el propio doc..
						TopDocs top=null;
						if (!titulo.equals("")){
							
							top = s.search(query, 2);
						}
						// copiaremos el mejor como el segundo, pero de no haber ponemos el primero (si mismo..), y sino un ""
						int mejor=0;
						if (titulo.equals("")||((!titulo.equals(""))&&(top.totalHits==0))){
							SimTitle.setStringValue("");
							SimTitle2.setStringValue("");
							SimBody.setStringValue("");
							SimBody2.setStringValue("");
							SimPathSgm.setStringValue("");
							SimPathSgm2.setStringValue("");
						}else 
						if (top.totalHits>1){
							
							mejor = top.scoreDocs[1].doc;
							
							SimTitle.setStringValue(s.doc(top.scoreDocs[0].doc).getField("tittle").stringValue());
							SimTitle2.setStringValue(s.doc(mejor).getField("tittle").stringValue());
							SimBody.setStringValue(s.doc(top.scoreDocs[0].doc).getField("body").stringValue());
							SimBody2.setStringValue(s.doc(mejor).getField("body").stringValue());
							SimPathSgm.setStringValue(s.doc(top.scoreDocs[0].doc).getField("pathSgm").stringValue());
							SimPathSgm2.setStringValue(s.doc(mejor).getField("pathSgm").stringValue());
						}else  {
							mejor = top.scoreDocs[0].doc; 
							
							SimTitle.setStringValue(s.doc(mejor).getField("tittle").stringValue());
							SimTitle2.setStringValue("");
							SimBody.setStringValue(s.doc(mejor).getField("body").stringValue());
							SimBody2.setStringValue("");
							SimPathSgm.setStringValue(s.doc(mejor).getField("pathSgm").stringValue());
							SimPathSgm2.setStringValue("");
							}
						
						
						Document doc = reader.document(i);
						
						doc.add(SimTitle);
						doc.add(SimTitle2);
						doc.add(SimBody);
						doc.add(SimBody2);
						doc.add(SimPathSgm);
						doc.add(SimPathSgm2);
						
						writer.addDocument(doc);
						
					
						
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			
		}
		
		
		
		
		@Override
		public void run() {
			try {
				
				String thread = Thread.currentThread().getName();
				String host = InetAddress.getLocalHost().getHostName();
				System.out.println("thread: "+ thread + " ,host: "+host);
				
				if (tipo==0){
					simDoc();
				}else{

					simBody();
				}
					try {
						writer.commit();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				

			} catch (Exception e) {
			
				e.printStackTrace();
			}

		}
	}

	static void similarDocTitle(DirectoryReader reader,Analyzer analyzer, IndexWriter writer,int numThreads,int tipo,int topN,String index_out) throws IOException {

		/*
		 * Create a ExecutorService (ThreadPool is a subclass of
		 * ExecutorService) with so many thread as cores in my machine. This can
		 * be tuned according to the resources needed by the threads.
		 */
		int primero =0;
		int ultimo =0;
		final int numCores = Runtime.getRuntime().availableProcessors();
		final ExecutorService executor = Executors.newFixedThreadPool(numCores);

		/*
		 * We use Java 7 NIO.2 methods for input/output management. More info
		 * in: http://docs.oracle.com/javase/tutorial/essential/io/fileio.html
		 *
		 * We also use Java 7 try-with-resources syntax. More info in:
		 * https://docs.oracle.com/javase/tutorial/essential/exceptions/
		 * tryResourceClose.html
		 */
	
		ArrayList<ResultTFIDF> results = null;
			int i =0;
			int grano = reader.numDocs()/numThreads;
			System.out.println("grano: "+grano);
			if (tipo==1){
				ProcessIndex p = new ProcessIndex();
				results = p.processIDFTF(reader, "body", topN, true);
			}
			for (i=0;i<numThreads;i++){
				primero = i*grano;
				if (i==(numThreads-1 )){
					ultimo = reader.numDocs();	
				}else {
					ultimo=(i*grano)+grano;
				}
				
				
					final Runnable worker = new WorkerThread(results,reader,analyzer,writer,primero,ultimo,tipo,topN, index_out);
					/*
					 * Send the thread to the ThreadPool. It will be processed
					 * eventually.
					 */
					executor.execute(worker);
				
			}
			
		/*
		 * Close the ThreadPool; no more jobs will be accepted, but all the
		 * previously submitted jobs will be processed.
		 */
		executor.shutdown();

		/* Wait up to 1 hour to finish all the previously submitted jobs */
		try {
			executor.awaitTermination(1, TimeUnit.HOURS);
		} catch (final InterruptedException e) {
			e.printStackTrace();
			System.exit(-2);
		}

		System.out.println("Finished all threads");

	}

}